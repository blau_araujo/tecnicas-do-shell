# Explicando os argumentos do `find`

> Dúvidas e discussões na issue #3

## Pregunta

*Olá Blau, tem alguns parametros no find que não entendo. Tipo o comando abaixo:*

```
find . -type d -name node_modules -exec rm -rf {} ;
```

*Essa parametro/expansão no final `{} ;`? O que exatamente ela faz?*

## Resposta

* `-type f` - retorna apenas nomes de arquivos comuns;
* `-type d` - retorna apenas nomes de diretórios;
* `-exec` - executa um dado comando, onde...
* `'{}'` - não é uma expansão, é um *placeholder* (guardador de lugar) para os argumentos e será substituído pelos nomes encontrados pelo find;
* `\;` - indica o término do comando a ser executado.


## Alguns detalhes importantes

* As chaves (`{}`) são escritas entre aspas simples para que os nomes de arquivos contendo espaços e caracteres especiais do shell sejam tratados como palavras únicas.
* O ponto e vírgula (`;`) precisa ser escapado para não ser confundido como o operador de controle utilizado entre de comandos do shell em uma mesma linha.
* A opção `-exec` pode receber o terminador `+` (em vez do `\;`). Assim, os nomes encontrados seriam **acumulados** numa lista de argumentos do comando, que seria executado apenas uma vez. Como  `;`, o comando é executado uma vez para cada argumento recebido.

Outra opção do `find` que pode ser interessante considerar é a `-maxdepth`, que limita os níveis de subdiretórios, contados a partir do diretório inicial informado, que serão pesquisados.