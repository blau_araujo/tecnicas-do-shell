#!/usr/bin/env bash

# Cálculo da raiz quadradada de inteiros
# pelo método das subtrações de ímpares.

sqrt() {
    until [[ $((number-sub)) -lt 0 ]]; do
	((prev=number, number-=sub, sub+=2, count++))
    done
    ((number == 0))
}

number=$1
fix=${2:-3}
sub=1
count=0

if sqrt; then
    echo $count
else
    out="$count."
    for ((n=1; n<=fix; n++)) {
	number=${number}00
	sub=$((sub-1))1
	count=0
	sqrt
	out+=$count
    }
    echo $out
fi
