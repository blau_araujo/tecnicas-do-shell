# Desafio: o terapeuta quântico

Desafio para a [transmissão de 19 de maio de 2022, às 20h UTC-3](https://codeberg.org/blau_araujo/tecnicas-do-shell/src/branch/main/lives/README.md).

## Descrição

O objetivo do desafio é construir um loop de conversas aleatórias com um
terapeuta artificial que responde de acordo com a percepção da entonação
do utilizador.

A entonação é determinada por padrões de texto e pode ser:

- Uma declaração normal;
- Uma declaração incisiva;
- Uma declaração agressiva;
- Uma pergunta incisiva;
- Uma pergunta normal.

Apenas a "pergunta normal" deverá produzir a saída de uma linha aleatória
do arquivo de "[frases de biscoito da sorte](https://codeberg.org/blau_araujo/tecnicas-do-shell/src/branch/main/desafios/terapeuta/sorte.txt)" (`sorte.txt`).

Antes de imprimir uma frase do arquivo, o programa deve tentar filtrar, com
o utilitário `grep`, as linhas que possuam ocorrências de um conjunto de
palavras-chave (vetor `normal_question`).

As respostas para as demais entonações detectadas devem ser escolhidas
aleatoriamente de seus respectivos vetores:

- declaração normal: `normal_statement`
- declaração incisiva: `yelling_statement`
- declaração agressiva: `all_caps`
- pergunta incisiva: `yelling_question`

## Requisitos

- Você pode utilizar qualquer utilitário disponível em sistemas GNU;
- O shell deve ser o Bash.
- O script deve ter opções de ajuda (`-h`) e versão (`-v`).
- O script é executado em loop (REPL) e deve ser terminado com a ocorrência de uma dessas palavras na linha digitada pelo usuário: `adeus`, `chega` `tchau` ou `fui`.

## Padrões de entonação

As condições para determinar os padrões de entonação estão nos comentários
de cada um dos vetores:

### normal_statement

```
# Condições:
# - Terminar com ponto ou palavra em minúsculas;
# - Maiúsculas apenas no início de frases.
normal_statement=(
    'Claro.'
    'Estou ouvindo.'
    'Entendo.'
    'Tem certeza?'
    'Tudo bem.'
    'Fale mais sobre isso.'
)
```

### yelling_statement

```

# Condições:
# - Terminar com exclamação ou palavra em caixa alta seguida de ponto;
# - Pelo menos uma palavra em caixa alta e uma em caixa baixa.
# - Não pode terminar com interrogação.
yelling_statement=(
    'Se você diz.'
    'Quem sou eu para questionar?'
    'Eu não teria tanta certeza.'
    'Não prefere fazer uma pergunta?'
    'Opa! Calma aí!'
    'Controle-se, por favor!'
)
```

### all_caps

```
all_caps=(
    'Eu não ganho para ouvir desaforos!'
    'Acho que você está me confundindo!'
    'Preciso chamar a segurança?'
    'Está tentando me intimidar?'
    'Respire fundo e tente novamente.'
    'Já ouviu falar em "netiqueta"?'
)
```

### yelling_question

```
# Condições:
# - Terminar com interrogação seguida ou não de exclamação;
# - Pelo menos uma palavra em caixa alta e uma em caixa baixa...
# - Ou última palavra em caixa alta seguida de interrogação.
yelling_question=(
    'Não se exalte se quiser uma resposta!'
    'Quem você acha que é?'
    'E você acha que eu sei?'
    'O que diz a sua consciência?'
    'Acalme-se e tente novamente.'
    'Quer um copo de água com açúcar?'
)
```

### normal_question

```
# Condições:
# - Terminar com interrogação;
# - Maiúsculas apenas no início de frases.
normal_question=(
    'amor|relacionamento'
    'alegria|felicidade'
    'amigo|amiga|amizade'
    'dinheiro|trabalho'
    'medo|ansiedade'
    'segredo|vida'
)
```

> **Nota:** fique à vontade para alterar as condições se achar que pode conseguir melhores resultados.
