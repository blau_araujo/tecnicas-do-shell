#!/usr/bin/env bash

# -----------------------------------------------------------------------------
# Função 1: trim()
# -----------------------------------------------------------------------------
# Uso      : trim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do inínicio e do fim da string
#            e atribui a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
trim() {
    :
}
# -----------------------------------------------------------------------------
# Função 2: ltrim()
# -----------------------------------------------------------------------------
# Uso      : ltrim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do início da string e atribui
#            a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
ltrim() {
    :
}
# -----------------------------------------------------------------------------
# Função 3: rtrim()
# -----------------------------------------------------------------------------
# Uso      : rtrim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do fim da string e atribui
#            a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
rtrim() {
    :
}
# -----------------------------------------------------------------------------
# Função 4: strim()
# -----------------------------------------------------------------------------
# Uso      : strim 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Remove caracteres do início e do fim da string,
#            trunca ocorrências múltiplas do mesmo caractere
#            na string (sqeeze) e atribui a uma variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
strim() {
    :
}
# -----------------------------------------------------------------------------
# Função 5: squeeze()
# -----------------------------------------------------------------------------
# Uso      : squeeze 'STRING' 'VAR' ['CARACTERE|CLASSE']
# Descrição: Trunca para uma as ocorrências múltiplas do
#            mesmo caractere na string e atribui a uma
#            variável.
#            Os caracteres padrão são os definidos pela
#            classe nomeada [:space:].
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
squeeze() {
    :
}
# -----------------------------------------------------------------------------
# Função 6: strlen()
# -----------------------------------------------------------------------------
# Uso      : strlen 'VAR'
# Descrição: Imprime o número de caracteres da string em VAR.
# Saída    : O número de caracteres da string.
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
strlen() {
    :
}
# -----------------------------------------------------------------------------
# Função 7: explode()
# -----------------------------------------------------------------------------
# Uso      : explode 'STRING' 'ARRAY' ['SEPARADOR|CLASSE']
# Descrição: Quebra a string nas ocorrências de um separador
#            e atribui cada parte a um elemento de um vetor.
#            O separador padrão é a vírgula.
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
explode() {
    :
}
# -----------------------------------------------------------------------------
# Função 8: implode()
# -----------------------------------------------------------------------------
# Uso      : implode 'VETOR' 'VAR' ['SEPARADOR']
# Descrição: Une os elementos de um vetor em uma string
#            intercalados por um separador e atribui
#            a uma variável.
#            O separador padrão é a vírgula.
# Saída    : Não produz dados na saída padrão
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
implode() {
    :
}
# -----------------------------------------------------------------------------
# Função 9: in_array()
# -----------------------------------------------------------------------------
# Uso      : in_array 'STRING|PADRÃO' 'VETOR'
# Descrição: Verifica se uma string ou padrão (FNPM)
#            acontece em algum dos elementos do vetor.
# Saída    : Não produz dados na saída padrão.
#            Define o vetor global ARRAY_MATCHES com os
#            índices dos elementos onde houve casamento.
#            Sai com sucesso se a string for encontrada.
#            Sai com erro 1 se a string não for encontrada
#            ou erro 2 se houver erro de parâmetros.
in_array() {
    :
}
# -----------------------------------------------------------------------------
# Função 10: in_string()
# -----------------------------------------------------------------------------
# Uso      : in_string 'STRING|PADRÃO' 'VAR'
# Descrição: Verifica se uma substring ou padrão (FNPM)
#            acontece em algum ponto da string em VAR.
# Saída    : Não produz dados na saída padrão.
#            Define o vetor global STRING_POS com os
#            índices dos caracteres na string a partir dos
#            quais houve casamento.
#            Sai com sucesso se a substring for encontrada.
#            Sai com erro 1 se a substring não for encontrada
#            ou erro 2 se houver erro de parâmetros.
in_string() {
    :
}
# -----------------------------------------------------------------------------
# Função 11: str_repeat()
# -----------------------------------------------------------------------------
# Uso      : str_repeat 'STRING|CHAR' REPETIÇÕES 'VAR'
# Descrição: Define uma variável associada ao resultado das
#            repetições de um caractere ou de uma string.
# Saída    : Não produz dados na saída padrão.
#            Sai com sucesso a menos que haja um erro de
#            parâmetros.
str_repeat() {
    :
}
# -----------------------------------------------------------------------------
# Função 12: count()
# -----------------------------------------------------------------------------
# Uso      : count 'VETOR'
# Descrição: Define a variável global ARRAY_COUNT com
#            a quantidade de elementos de um vetor.
# Saída    : Não produz dados na saída padrão.
#            Sai com erro 1 se a variável não for um
#            vetor ou com erro 2 se houver erro de
#            parâmetros.
count() {
    :
}
