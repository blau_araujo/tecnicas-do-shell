# Bash String Library (bstrlib)

Observando as condições e descrições, crie as 12 funções propostas para manipulação de strings e dados em vetores.

## Objetivos

- Exercitar técnicas para manipular strings e vetores.
- Explorar os escopos das variáveis nos scripts.
- Entender melhor o conceito de "retorno" em funções do Bash.
- Desenvolver a capacidade de pesquisar soluções e explicá-las.
- Consolidar a visão da criação de scripts em Bash como programação.

## Prêmios

- Uma cópia do [Pequeno Manual do Programador GNU/Bash](https://blauaraujo.com/livro) para o script melhor avaliado.
- Todos os participantes receberão um [convite gratuito para o Jit.si](https://codeberg.org/blau_araujo/tecnicas-do-shell/src/branch/main/lives/README.md#convites-para-o-jitsi) no episódio da apresentação dos resultados deste desafio.
- Independente da classificação, as funções mais interessantes serão apresentadas e aprofundadas em vídeos individuais no canal debxp.

### Criérios de avaliação

- Observância das condições e descrições.
- Data da entrega (quanto antes, mais pontos).
- Clareza e riqueza das explicações dos comentários.
- Síntese das soluções (soluções abreviadas valem mais pontos).
- Engenhosidade.

### Importante!

Havendo menos de 3 participantes, o desafio será cancelado!

## Condições

- Soluções 100% em Bash.
- Uma função não pode depender das outras.
- Funções que pedirem variáveis entre aspas como argumentos referem-se aos nomes dessas variáveis, não às suas expansões.
- As formas de uso e as descrições devem ser seguidas à risca.
- Deve ser utilizado o [arquivo original do script](https://codeberg.org/blau_araujo/tecnicas-do-shell/src/branch/main/desafios/bstrlib/bstrlib.sh) alterando apenas o conteúdo das funções.
- Cada linha relevante das funções deve ser explicada em comentário.
- As soluções devem ser apresentadas nos repositório próprios dos autores no Codeberg: https://codeberg.org
- Não serão aceitas soluções apresentadas em outras plataformas.
- O link do repositório deve ser informado na [issue relativa a este desafio](https://codeberg.org/blau_araujo/tecnicas-do-shell/issues/22).

## Descrições das funções

As descrições das funções estão nos comentários do [arquivo do script](https://codeberg.org/blau_araujo/tecnicas-do-shell/src/branch/main/desafios/bstrlib/bstrlib.sh).

## Data limite de entrega

Sexta-feira, 3 de junho de 2022.
