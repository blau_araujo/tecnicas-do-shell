#!/bin/bash
version=0.0.1
usage='
  Usage: round.sh [MODO] NUMBER

DESCRIPTION
  Script que faz o arredondamento de NUMBER
  
OPTIONS
  MODO       Modos round, ceil ou floor
             round
             O valor arredondado para o inteiro mais próximo, seja ele maior ou menor.
             floor
             O valor arredondado para o menor valor inteiro anterior.
             ceil
             O valor arredondado para o maior valor inteiro seguinte.
  NUMBER     Números inteiros, decimais, positivos ou negativos são aceitos. Vírgulas 
             e pontos são acitos como ponto decimal. Pontos de milhar não são aceitos

'

is_valid_number() [[ $1 =~ ^(-)?[0-9]+([.,])?([0-9]*)?$ ]]

round() {
  local fator
  if [[ $1 =~ ^((-)?[0-9]+)([,.])?([0-9]*) ]]; then
    [[ ${BASH_REMATCH[4]::1} -ge 5 ]] && fator=${BASH_REMATCH[2]}1
    echo $((${BASH_REMATCH[1]} + fator))
  fi
}

ceil() {
  local fator
  if [[ $1 =~ ^((-)?[0-9]+)([,.])?([0-9]*) ]]; then
    [[ -z ${BASH_REMATCH[2]} && ${BASH_REMATCH[4]} -gt 0 ]] && fator='1'
    echo $((${BASH_REMATCH[1]} + fator))
  fi
}

floor() {
  local fator
  if [[ $1 =~ ^((-)?[0-9]+)([,.])?([0-9]*) ]]; then
    [[ ${BASH_REMATCH[2]} && ${BASH_REMATCH[4]} -gt 0 ]] && fator='-1'
    echo $((${BASH_REMATCH[1]} + fator))
  fi
}

[[ $1 == @(-v|--version) ]] && { echo $version; exit; }
[[ $1 == @(-h|--help) ]] && { echo "$usage"; exit; }
mode=round
[[ $1 == @(round|ceil|floor) ]] && { mode=$1; shift; }
if is_valid_number "$1"; then
  $mode "$1"
fi
