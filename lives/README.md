# Técnicas do Shell - ao vivo

<!--more-->

**A transmissão de hoje foi reagendada para a quinta que vem, 2 de junho!**

## Nesta quinta (02/06/2022)

Parte 2 das soluções para o desafio do [terapeuta quântico (ELIZA)](https://codeberg.org/blau_araujo/tecnicas-do-shell/src/branch/main/desafios/terapeuta).

### Repósitório da série

- https://codeberg.org/blau_araujo/tecnicas-do-shell

### Para assistir ao vivo

- [Youtube](https://youtu.be/anQnEVrvOmM)
- [Pelo meu site](https://blauaraujo.com/lives)
- Pelo MPV: `mpv https://blauaraujo.com/lives`

### Para interagir ao vivo

- Chat da transmissão no Youtube
- [Chat no Gitter](https://gitter.im/blau_araujo/community) (permite melhor edição de códigos)
- Pelo Jitsi (apenas por convite)

### Para assistir gravado

- [Playlist dos cortes no Youtube](https://youtube.com/playlist?list=PLXoSGejyuQGrUv3tv46y3FSrP-pz3fSv_).
- Somente os participantes pelo Jitsi terão acesso à gravação bruta depois da transmissão e até a publicação dos cortes.
- Os demais poderão assistir aos cortes dos momentos mais relevantes gratuitamente pelo Youtube.

## Convites para o Jitsi

- Todos os apoiadores regulares (Apoia.se) podem solicitar gratuitamente o link do convite pelo e-mail [blau@debxp.org](mailto:blau@debxp.org).
- Convite para quatro transmissões): R$150,00
- Convite para uma transmissão: R$50,00

### Formas de contribuição para obter o convite

- [Apoio regular no Apoia.se](https://apoia.se/debxpcursos)
- [Depósito no PicPay](https://app.picpay.com/user/blauaraujo)
- Chave PIX: pix@blauaraujo.com

**Não se esqueça de mandar um e-mail solicitando o convite para o Jitsi!**
